﻿using UnityEngine;
using System.Collections;

public abstract class Manager : MonoBehaviour {

    //Manager specific update
	public abstract void UpdateManager();

    //Change to game state
    public virtual void GoToGameState (){}

    //Change to menu state
    public virtual void GoToMenuState(){}
}
