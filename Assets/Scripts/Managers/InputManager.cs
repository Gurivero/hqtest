﻿using UnityEngine;
using System.Collections;

//class that manages the controllers for each player. In this version it only has one player support
//but it can be easily changed to support multiplayer
public class InputManager : Manager {

    public Controller currentController{ get; private set; }
       
    public override void UpdateManager()
    {
        currentController.UpdateController ();
    }

    public override void GoToGameState()
    {
        //set up in-game player controller
        currentController.SetupControllerMapping ();
    }

    public override void GoToMenuState()
    {
        //if a menu implementation was done, here we would set the menu controller
    }

    public void AssignController(Player p)
    {
        //in a final version this should be changed to accomodate several players
        currentController = p.gameObject.AddComponent< Controller > ();
        currentController.AttachPlayer (p);
    }
}