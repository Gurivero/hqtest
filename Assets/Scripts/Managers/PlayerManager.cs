﻿using UnityEngine;
using System.Collections;

//class that manages the players. Right now it is built around having only one player,
//but can easily be changed to support multiplayer
public class PlayerManager : Manager {

    [SerializeField]
    GameObject _charPrefab;

    Player _player;

    public GameObject charPrefab{ get { return _charPrefab; }}
    public Player player{ get{ return _player; }}

    public override void UpdateManager()
    {
        _player.UpdatePlayer ();
    }

    public void CreatePlayer()
    {
        if(_player == null)
        {
            GameObject go = Master.instance.CreateGameObject ("Player", transform);
            _player = go.AddComponent< Player > ();
            Master.instance.inputManager.AssignController(_player);
        }
    }
}
