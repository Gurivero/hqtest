﻿using UnityEngine;
using System.Collections;

//character in the game that is controlled by a player
public class Character : MonoBehaviour {

    [Header("Physics Settings")]

    [SerializeField]
    float _walkForce;

    [SerializeField]
    float _runForce;

    [SerializeField]
    float _maxVelocityWalk;

    [SerializeField]
    float _maxVelocityRun;

    bool _isRunning = false;

    Animator _animator;
    Rigidbody2D _rBody;
    Controller _controller;

    void Start () 
    {
        _animator = GetComponent< Animator > ();
        _rBody = GetComponent< Rigidbody2D > ();
    }

    //the player animation parameters are mainly changed here
    public void UpdateCharacter()
    {
        _animator.SetFloat ("Speed", _rBody.velocity.magnitude);
        _animator.SetBool ("Run", _isRunning);

        //this is needed since the run is press&hold
        _isRunning = false;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        _animator.SetBool ("Ground", true);
    }

    public void SetController(Controller c)
    {
        _controller = c;
    }

    void MoveCharacter(float force, float maxVel)
    {
        //only add force if the velocity does not exceed the maximum value allowed
        if(_rBody.velocity.magnitude < maxVel)
        {
            Vector2 dir = _controller.inputAxis;

            //we don't want vertical movement
            dir.y = 0f;

            //make the walk force frame independent
            dir *= force * Time.deltaTime;

            _rBody.AddForce (dir);

            //update character rotation
            UpdateRotation ();
        }
    }

    void UpdateRotation()
    {
        //if the input axis is zero, don't update rotation, since Quaternion.LookRotation logs an error when the vector is zero
        if(_controller.inputAxis != Vector2.zero)
        {
            Vector3 dirToFace = Vector3.zero;
            dirToFace.z = _controller.inputAxis.x;
            dirToFace = dirToFace.normalized;
            transform.rotation = Quaternion.LookRotation (dirToFace);
        }
    }

    public void Walk ()
    {
        MoveCharacter (_walkForce, _maxVelocityWalk);
    }

    public void Run()
    {
        _isRunning = true;
        MoveCharacter (_runForce, _maxVelocityRun);
    }

    public void Teleport(float distance)
    {
        Vector2 origin = new Vector2(transform.position.x, transform.position.y);
        Vector2 direction = new Vector2(transform.forward.z, 0f);

        RaycastHit2D hit = Physics2D.Raycast (origin, direction, distance);

        if(hit.collider != null)
        {
            switch(hit.collider.tag)
            {
            case "Enemy":
                //special enemy teleport case
                TeleportBehindEnemy (hit);
                break;
            default:
                Vector3 pos = hit.point;
                GoToTPPosition (pos);
                break;
            }
        }
        else //no obstacles found during teleport
        {
            Vector3 newpos = transform.position;

            //multiply the teleport distance by direction
            newpos.x += distance * transform.forward.z;

            GoToTPPosition (newpos);
        }
    }

    void TeleportBehindEnemy(RaycastHit2D hit)
    {
        float enemyDistance = hit.distance;
        Collider2D enemyCollider = hit.collider;

        Transform enemyTransform = hit.collider.transform;
        Vector2 origin = new Vector2(enemyTransform.position.x, enemyTransform.position.y);
        Vector2 direction = new Vector2(transform.forward.z, 0f);

        //check behind the enemy for available spot to teleport
        RaycastHit2D rHit = Physics2D.Raycast (origin, direction, enemyDistance/2f);   

        float teleportDirection = transform.forward.z;

        //only teleport if no obstacle was found
        if(rHit.collider == null)
        {
            Vector3 newpos = enemyTransform.position;

            //multiply the teleport distance by direction
            float distanceToTravel = (enemyDistance/2f) * teleportDirection;

            newpos.x += distanceToTravel;

            //check if the teleport was performed too close to the enemy
            if(enemyCollider.bounds.Contains (newpos))
            {
                newpos = enemyTransform.position;
                newpos.x += enemyCollider.bounds.extents.x * teleportDirection;
            }

            GoToTPPosition (newpos);
        }
    }

    //teleport the character to the intended teleport position
    void GoToTPPosition(Vector3 pos)
    {
        Vector3 newpos = transform.position;
        newpos.x = pos.x;

        transform.position = newpos;
    }
}
