﻿using UnityEngine;
using System.Collections;

//entity that represents a player (person)
public class Player : MonoBehaviour {

    Character _character;
    Utility _activeUtility;
    Transform _utilityParent;

    public Controller controller{ get; private set; }
    public Character character{ get { return _character; }}


    public void UpdatePlayer()
    {
        _character.UpdateCharacter ();
    }

    public void SetController(Controller c)
    {
        controller = c;
    }

    public void SetCharacter(Character c)
    {
        _character = c;
        _character.SetController(controller);
        c.transform.parent = transform;
        c.name = "Char" + name;
    }

    public void SetActiveUtility(Utility u)
    {
        if(_activeUtility != null) //remove previous utility
        {
            _activeUtility.DestroyUtility ();
        }

        _activeUtility = u;

        //Set the utility as a child of the character
        RelocateUtility ();
    }

    void RelocateUtility()
    {
        _activeUtility.transform.parent = _character.transform;
        _activeUtility.transform.localPosition = Vector3.zero;
    }

    public void UseUtility()
    {
        if(_activeUtility != null)
        {
            _activeUtility.Use ();
        }
        else
        {
            Debug.Log ("active utility not set.");
        }
    }
}
