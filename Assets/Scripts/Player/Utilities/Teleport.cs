﻿using UnityEngine;
using System.Collections;

public class Teleport : Utility {

    [SerializeField]
    float _teleportDistance;

	public override void Use()
    {
        _player.character.Teleport(_teleportDistance);
    }
}
