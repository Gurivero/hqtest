﻿using UnityEngine;
using System.Collections;

public abstract class Utility : MonoBehaviour {

    protected Player _player;
    
    void Start()
    {
        AttachToPlayer ();
    }

    void AttachToPlayer()
    {
        _player = transform.GetComponentInParent< Player > ();

        if(_player == null) //prefab was instantiated in a erroneous location
        {
            FindPlayerAndAttach ();
        }

        //set this utility as the current utility of a player
        _player.SetActiveUtility (this);
    }

    //this only works when having one player
    void FindPlayerAndAttach()
    {
        _player = Master.instance.playerManager.player;
    }

    public void DestroyUtility()
    {
        Destroy (gameObject);
    }

    public abstract void Use();
}
