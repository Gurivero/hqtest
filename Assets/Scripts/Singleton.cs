﻿using UnityEngine;
using System.Collections;

//Singleton class based in the Singleton class from Unity wiki
public class Singleton< T > : MonoBehaviour where T : MonoBehaviour {

    static T _instance;

    public static T instance
    { 
        get 
        {
            if( _instance == null) 
            {
                _instance = (T)FindObjectOfType(typeof(T)); // check for existing instance
                         
                if(_instance == null) // no instance present, create one
                {
                    GameObject singleton = new GameObject();
                    singleton.AddComponent< T >();
                    singleton.name = typeof(T).ToString ();
                    _instance = singleton.GetComponent< T >();

                    DontDestroyOnLoad (singleton);
                }
            }

            return _instance;

        } 
    }
}
