﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

    protected Command _axis;
    protected Command _button0;
    protected Command _button1;

    protected Vector2 _inputAxis;

    public Vector2 inputAxis{ get{ return _inputAxis;} }
    public Player  player{ get; private set; }


    public void AttachPlayer(Player p)
    {
        player = p;
        player.SetController(this);
    }

    public void UpdateController()
    {
        _inputAxis.x = Input.GetAxis ("Horizontal");
        _inputAxis.y = Input.GetAxis ("Vertical");

        ManageButtonPress (_button0, "Button0");  //use active utility
        ManageButtonPress (_button1, "Button1");  //check if the run key is being pressed to decide between walk and run commands

        ManageAxisPress (_axis);  //only move the character if the input axis is being pressed    
    }

    void ManageButtonPress(Command c, string keyName)
    {
        if(Input.GetButtonDown (keyName)) //action executed when pressing down the button
        {
            c.ExecuteOnDown (this);
        }
        else if(Input.GetButtonUp (keyName)) //action executed when releasing the button
        {
            c.ExecuteOnUp (this);
        }
        else if(Input.GetButton (keyName)) //action executed while holding the button
        {
            c.ExecuteOnHold (this);
        }
    }

    void ManageAxisPress(Command c)
    {
        _inputAxis.x = Input.GetAxis ("Horizontal");
        _inputAxis.y = Input.GetAxis ("Vertical");

        if(_inputAxis.magnitude > 0 ) //only execute action if the axis is not in the default position
        {
            c.ExecuteOnHold (this); 
        }
    }

    public void SetupControllerMapping()
    {
        switch(Master.instance.estate)
        {
        case Master.ESTATE.GAME:
            SetupGameMapping ();
            break;
        case Master.ESTATE.MENU:
            SetupMenuMapping ();
            break;
        }
    }

    void SetupGameMapping()
    {
        _axis = new CommandWalk();
        _button0 = new CommandUseUtility();
        _button1 = new CommandRun();
    }

    void SetupMenuMapping()
    {
        //not used in this coding test
    }
}
