﻿using UnityEngine;
using System.Collections;

public class CommandRun : Command {

    public override void ExecuteOnHold(Controller c)
    {
        c.player.character.Run ();
    }
}
