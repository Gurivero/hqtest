﻿using UnityEngine;
using System.Collections;

public abstract class Command {

    //execute this command when ending a button press
	public virtual void ExecuteOnUp(Controller c){}

    //execute this command when pressing down a button
    public virtual void ExecuteOnDown(Controller c){}

    //execute this command while holding a button down
    public virtual void ExecuteOnHold(Controller c){}
}
