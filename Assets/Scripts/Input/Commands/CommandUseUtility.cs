﻿using UnityEngine;
using System.Collections;

public class CommandUseUtility : Command {

	public override void ExecuteOnUp (Controller c)
    {
        c.player.UseUtility ();
    }
}
