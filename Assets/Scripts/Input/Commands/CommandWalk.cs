﻿using UnityEngine;
using System.Collections;

public class CommandWalk : Command {
    
	public override void ExecuteOnHold(Controller c)
    {
        c.player.character.Walk();
    }
}
