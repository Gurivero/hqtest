﻿using UnityEngine;
using System.Collections;

//class responsible for in-game state behaviour
public class GameState : State {

    void Start()
    {
        SpawnPlayer (Master.instance.playerManager.player);
    }

    public override void UpdateState()
    {
        //state related operations are updated here
    }

    void SpawnPlayer(Player p)
    {
        GameObject go = Instantiate (Master.instance.playerManager.charPrefab);
        go.transform.position = new Vector3(0,5,0);
        p.SetCharacter (go.GetComponent< Character > ());
    }
}
