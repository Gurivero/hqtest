﻿using UnityEngine;
using System.Collections;

//base class for the game states. In this test we only have one state: GameState
public abstract class State : MonoBehaviour {

    //frame update
	public abstract void UpdateState();
}
