﻿using UnityEngine;
using System.Collections;

public class Master : Singleton< Master > {

    public enum ESTATE
    {
        GAME,
        MENU
    }

    //the current state of the game
    public State state{ get; private set; }
    public ESTATE estate{ get; private set; }

    //the managers used in the game
    public InputManager inputManager{ get; private set; }
    public PlayerManager playerManager{ get; private set; }
   
    GameObject _stateGO;


    void Awake () 
    {
        InitializeChildren ();

        ChangeState (ESTATE.GAME);
	}

    // Update is called once per frame
    void Update () 
    {
        //By calling a custom update method in the managers we reduce the number of expensive calls
        //to the in-built Unity Update method

        inputManager.UpdateManager ();
        playerManager.UpdateManager ();

        //update current state
        state.UpdateState ();
    }

    void InitializeChildren()
    {
        //create empty child to contain the current state
        _stateGO = CreateGameObject ("State", transform);

        inputManager  = GetComponentInChildren< InputManager > ();
        playerManager = GetComponentInChildren< PlayerManager> ();

        //create one player
        playerManager.CreatePlayer();
    }

    //Create GameObject with name <name> and parent <parent>
    public GameObject CreateGameObject(string name, Transform parent = null)
    {
        GameObject go = new GameObject ();
        go.name = name;
        go.transform.parent = parent;
        return go;
    }

    //adds a component State to the _stateGO object
    void AddState(System.Type stateType)
    {
        state = (State) _stateGO.AddComponent (stateType);
    }

    public void ChangeState(ESTATE s)
    {
        //destroy the current state before initializing the new state
        if(state != null)
        {
            Destroy (state);
        }

        estate = s;

        switch(s)
        {
        case ESTATE.GAME:
            GoToGameState ();
            break;
        case ESTATE.MENU:
            GoToMenuState ();
            break;
        default:
            break;
        }
    }

    void GoToGameState()
    {
        AddState (typeof(GameState));

        inputManager.GoToGameState ();
    }

    void GoToMenuState()
    {
        inputManager.GoToMenuState ();
    }
}